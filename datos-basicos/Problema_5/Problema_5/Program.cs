﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problema_5
{
    class Program
    {
        static void Main(string[] args)
        {
            decimal a = 5;
            Console.WriteLine("valor de a: " + a);
            decimal b = ++a;
            Console.WriteLine("valor de b: " + b);
            decimal c = a++;
            Console.WriteLine("valor de c: " + c);
            decimal b2 = b * 5;
            Console.WriteLine("valor de b2: " + b2);
            decimal a2 = a * 2;
            Console.WriteLine("valor de a2: " + a2);


        }
    }
}
