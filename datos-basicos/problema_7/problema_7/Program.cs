﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace problema_7
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("ingrese la distancia recorrida en  el metro ");
            int metros = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("ingrese la cantidad de horas en la que fue recorrida");
            int minutos = Convert.ToInt32(Console.ReadLine());

            int segundos = (minutos * 60 * 60);

            int velocidad = metros / segundos;
            Console.WriteLine($" su velocidades es {velocidad}m/s ");

            double kilometros = (metros / 1000);
            Console.WriteLine($" kilometros {kilometros}Km/h ");

            double millas = (kilometros / 1.609);
            Console.WriteLine($" millas {millas}m/h ");

            Console.ReadKey();

        }
    }
}
